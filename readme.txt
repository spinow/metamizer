
What is Metamizer?
Metamizer is intended to be a free api used by audio players.
It consists in a library that processes audio files, filling them with information about music characteristics like speed and mood.

Why Metamizer?
Have you ever been listening to music and as your mood was not that good, so you were constantly skipping songs to listen only some kind of sad ones?
Metamizer organizes music information such as speed and mood, so users could change their playlist based how they have woken up.
